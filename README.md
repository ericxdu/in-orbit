# In Orbit

## About

In Orbit is a simple, calming test app for LibRetro/RetroArch using the Lutro core.

## Setup

If you only have source, put all the files into a new .zip file and name the file "In Orbit.lutro".

Put "In Orbit.lutro" in roms/lutro on your RetroArch device and scan the folder using the RetroArch "Add" tab. The app should now be listed under the Lutro playlist. Start the app using the core "Lua Engine (Lutro)".

## Usage

In Orbit immediately displays the calming view of a spaceship in orbit of a planet, along with a chill orchestral audio track. The controls are as follows.

  - South/B: stop music track
  - West/Y/Start: play music track
  - East/A: pause music track
  - Select: change spaceship
  - L1/R1: rotate spaceship
