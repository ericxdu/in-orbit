--In Orbit - a relaxing idle video game simulator for LibRetro
--Copyright 2021 Eric Duhamel

--This program is free software: you can redistribute it and/or modify
--it under the terms of the GNU General Public License as published by
--the Free Software Foundation, either version 3 of the License, or
--(at your option) any later version.

--This program is distributed in the hope that it will be useful,
--but WITHOUT ANY WARRANTY; without even the implied warranty of
--MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--GNU General Public License for more details.

--You should have received a copy of the GNU General Public License
--along with this program.  If not, see <https://www.gnu.org/licenses/>.

function lutro.load()
  math.randomseed(os.time())
  SHIP = {img = lutro.graphics.newImage("images/ship00.png"),
          qua = lutro.graphics.newQuad(0, 0, 256, 256, 4096, 768),
          hull = 0, dir = 100, x = 40, y = 5, dx = 1, dy = 5}
  BG_IMG = lutro.graphics.newImage("images/back00.png")
  TRACKS = {
    lutro.audio.newSource("music/track_00.ogg", 'stream'),
    lutro.audio.newSource("music/track_01.ogg", 'stream'),
    lutro.audio.newSource("music/track_02.ogg", 'stream'),
    lutro.audio.newSource("music/track_03.ogg", 'stream'),
    lutro.audio.newSource("music/track_04.ogg", 'stream')
  }
  BGM = TRACKS[math.random(1, #TRACKS)]
end

function lutro.update(dt)
  if not BGM:isPlaying() then
    BGM = TRACKS[math.random(1, #TRACKS)]
    BGM:play()
  end
  if SHIP.x < 32 then SHIP.dx = SHIP.dx + 0.01
  elseif SHIP.x > 48 then SHIP.dx = SHIP.dx - 0.01 end
  if SHIP.y < 5 then SHIP.dy = SHIP.dy + 0.1
  elseif SHIP.y > 10 then SHIP.dy = SHIP.dy - 0.1 end
  SHIP.x = SHIP.x + SHIP.dx*dt SHIP.y = SHIP.y + SHIP.dy*dt
  if TRIG_S then BGM:stop()
  elseif TRIG_W then BGM:play()
  elseif TRIG_SL then SHIP.hull, TRIG_SL = (SHIP.hull+1)%3, false
  elseif TRIG_ST then BGM:play()
  elseif TRIG_E then BGM:pause()
  elseif TRIG_TL then SHIP.dir = SHIP.dir + 500*dt
  elseif TRIG_TR then SHIP.dir = SHIP.dir - 500*dt
  end
  SHIP.dir = SHIP.dir % 360
  SHIP.qua:setViewport(math.floor(SHIP.dir/22.5)*256, (SHIP.hull)*256, 256, 256)
end

function lutro.draw()
  lutro.graphics.scale(2.5)
  local x, y = (320-BG_IMG:getWidth())/2, (240-BG_IMG:getHeight())/2
  lutro.graphics.draw(BG_IMG, x, y)
  lutro.graphics.draw(SHIP.img, SHIP.qua, SHIP.x, SHIP.y)
end

function lutro.joystickpressed(n, b)
  if b == 0 then TRIG_S = true
  elseif b == 1 then TRIG_W = true
  elseif b == 2 then TRIG_SL = true
  elseif b == 3 then TRIG_ST = true
  elseif b == 8 then TRIG_E = true
  elseif b == 9 then TRIG_N = true
  elseif b == 10 then TRIG_TL = true
  elseif b == 11 then TRIG_TR = true
  end
end

function lutro.joystickreleased(n, b)
  if b == 0 then TRIG_S = false
  elseif b == 1 then TRIG_W = false
  elseif b == 2 then TRIG_SL = false
  elseif b == 3 then TRIG_ST = false
  elseif b == 8 then TRIG_E = false
  elseif b == 9 then TRIG_N = false
  elseif b == 10 then TRIG_TL = false
  elseif b == 11 then TRIG_TR = false
  end
end
